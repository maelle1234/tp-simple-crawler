# TP-simple-crawler



## Description du projet

Ce projet est réalisé dans le cadre d'un TP d'Indexation Web à l'ENSAI. L'objectif est de construire un crawler simple qui à partir d'une URL donnée (https://ensai.fr/), récupère 50 nouvelles URLs et les stocke dans un fichier texte, tout en respectant les règles définies par les fichiers robots.txt ainsi que la politesse. L'implémentation est réalisée en Python.

## Exécution du code

```
git clone https://gitlab.com/maelle1234/tp-simple-crawler.git
cd tp-simple-crawler
pip install -r requirements.txt
python3 main.py
```

## Fonctionnement du code

Le crawler est implémenté dans la classe `myCrawler`, définie dans le fichier `my_crawler.txt`. Cette classe se compose de plusieurs méthodes utiles à l'éxecution du crawler, de la méthode `launch_crawler` qui permet de lancer le processus, et d'une méthode pour sauvegarder le résultat obtenu (la liste d'URLs) dans un fichier texte. Les consignes données sont de respecter 5s avant de télécharger une nouvelle page et d'obtenir un nombre maximal de 50 URLs, mais ces paramètres (par défaut) peuvent être modifiés lors de l'instanciation de `myCrawler`.

Le code permettant de lancer le crawler avec les paramètres par défaut est contenu dans le fichier `main.py`. 

Des test unitaires ont été réalisés et sont présents dans `tests/test_my_crawler.py`. Pour les lancer, on peut exécuter la commande suivante:

```
python3 -m unittest
```

### Contributeurs

Maëlle Cosson