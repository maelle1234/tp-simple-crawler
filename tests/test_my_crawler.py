import unittest
from my_crawler import myCrawler

class TestMyCrawler(unittest.TestCase):

    def test_get_domain_from_url_page(self):
        url1 = 'https://ensai.fr/'
        url2 = 'https://twitter.com/ensai35?lang=fr'
        crawler = myCrawler('url_test')
        domain1 = crawler.get_domain_from_url_page(url1)
        domain2 = crawler.get_domain_from_url_page(url2)
        self.assertEqual(domain1, 'https://ensai.fr/')
        self.assertEqual(domain2, 'https://twitter.com/')
        
    def test_can_fetch_page(self):
        url_domain1 = 'https://ensai.fr/'
        url_page1 = 'https://ensai.fr/politique-de-confidentialite/'
        url_domain2 = 'https://www.facebook.com/'
        url_page2 = 'https://www.facebook.com/Ensai35/'
        crawler = myCrawler('url_test')
        res1 = crawler.can_fetch_page(url_domain1, url_page1)
        res2 = crawler.can_fetch_page(url_domain2, url_page2)
        self.assertTrue(res1)
        self.assertFalse(res2)

    def test_get_page_content(self):
        url_page = 'https://ensai.fr/'
        crawler = myCrawler('url_test')
        content = crawler.get_page_content(url_page)
        self.assertIn('html', content)

    def test_find_urls(self):
        # exemple de document html simple trouvé sur internet
        content = '<!DOCTYPE html><html><body><h2>HTML Links</h2><p>HTML links are defined with the a tag:</p><a href="https://www.w3schools.com">This is a link</a></body></html>'
        crawler = myCrawler('url_test')
        urls_found = crawler.find_urls(content)
        self.assertIn('https://www.w3schools.com', urls_found)
        self.assertEqual(len(urls_found),1)

    def test_get_site_maps(self):
        url_domain = 'https://ensai.fr/'
        crawler = myCrawler('url_test')
        res = crawler.get_list_site_maps(url_domain)
        #print(res)
        self.assertIn('http://ensai.fr/sitemap_index.xml', res)
