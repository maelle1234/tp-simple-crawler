import time
import socket
import urllib.robotparser
import urllib.request
from urllib.parse import urlparse
from bs4 import BeautifulSoup


class myCrawler:

    """ Create a simple crawler.
    
    Attributes
    ----------
    initial_url : str
        seed of the crawler, first URL to inspect
    sleep_time : int
        time to wait between two URL requests, in seconds
    max_urls : int
        maximum number of URLs to reach by the crawler
    all_urls : list
        URLs found by the crawler
    next_urls : list
        URLs to inspect next
    """
    
    def __init__(self, initial_url, sleep_time=5, max_urls=50):
        self.initial_url = initial_url
        self.sleep_time = sleep_time
        self.max_urls = max_urls
        self.all_urls = [] #output, doit être de taille 50 (max_urls) à la fin 
        self.next_urls = [] 


    def get_domain_from_url_page(self, url_page):
        """ Returns the domain part of any given URL, to access the robots.txt file """
        res_parser = urlparse(url_page)
        domain = res_parser.netloc
        protocol = res_parser.scheme
        domain_formatted = '{}://{}/'.format(protocol, domain)
        return domain_formatted


    def can_fetch_page(self, url_domain, url_page):
        """ Return a boolean that indicates if we can crawl this page or not, using information
        in the robots.txt file """
        url_robots =  url_domain + 'robots.txt' #ex: https://ensai.fr/robots.txt
        try:
            urllib.request.urlopen(url_robots, timeout=2)
        except urllib.error.URLError:
            res = False
        except socket.timeout: # erreur rencontrée pour https://www.datastorm.fr/
            res = False
        except ValueError:
            res = False
        else:
            rp = urllib.robotparser.RobotFileParser()
            rp.set_url(url_robots)
            rp.read()
            res = rp.can_fetch('*', url_page)
        return res


    def get_list_site_maps(self, url_domain):
        """ Return the list of sitemaps found in the robots.txt file of the given site """
        url_robots =  url_domain + 'robots.txt' #ex: https://ensai.fr/robots.txt
        try:
            urllib.request.urlopen(url_robots, timeout=2)
        except urllib.error.URLError:
            res = False
        except socket.timeout: # erreur rencontrée pour https://www.datastorm.fr/
            res = False
        except ValueError:
            res = False
        else:
            rp = urllib.robotparser.RobotFileParser()
            rp.set_url(url_robots)
            rp.read()
            res = rp.site_maps()
        return res


    def get_sitemap_content(self, url_sitemap):
        """ Return the content of the sitemap, using BeautifulSoup to parse XML """
        try:
            response = urllib.request.urlopen(url_sitemap, timeout=2)
        except urllib.error.URLError:
            sitemap_content = ''
        except socket.timeout: # erreur rencontrée pour https://www.datastorm.fr/
            sitemap_content = ''
        except ValueError:
            sitemap_content = ''
        else:
            sitemap_content = BeautifulSoup(response, 'lxml-xml', from_encoding=response.info().get_param('charset'))
        return sitemap_content


    def get_sitemap_type(self, sitemap_content):
        """ Return the type of a given sitemap, which can be an index linking to other sitemaps, or a sitemap that lists URLs """
        type = ''
        if 'sitemapindex' in sitemap_content:
            type = 'sitemapindex'
        elif 'urlset' in sitemap_content:
            type = 'urlset'
        return type

    
    def find_urls_from_sitemap(self, sitemap_content):
        """ Return all URLs found in a sitemap file """
        pass


    def get_page_content(self, url_page):
        """ Open, read and return content from a html page as a string, given its URL """
        try:
            with urllib.request.urlopen(url_page, timeout=2) as f:
                page_content = f.read().decode('utf-8')
        except urllib.error.URLError:
            page_content = ''
        except socket.timeout: # erreur rencontrée pour https://www.datastorm.fr/
            page_content = ''
        except ValueError:
            page_content = ''
        return page_content


    def find_urls(self, page_content):
        """ Given a page content as a string, find and return as a list all URLs on that page """
        if len(page_content) != 0:
            soup = BeautifulSoup(page_content, 'html.parser')
            found_urls = []
            for link in soup.find_all('a'):
                found_urls.append(link.get('href'))
        else:
            found_urls = []
        return found_urls


    def launch_crawler(self):
        """ Launch crawler, starting with the specified seed """
        # on regarde d'abord la page initiale (seed)
        self.next_urls = [self.initial_url]
        for url in self.next_urls:
            if len(self.all_urls) < self.max_urls:
                print('Current URL being checked is: {}'.format(url))
                # on vérifie qu'on peut crawler la page, qu'on n'a pas encore atteint la limite
                # et que la page n'a pas encore été crawlée
                url_domain_current_page = self.get_domain_from_url_page(url)
                if self.can_fetch_page(url_domain_current_page, url) \
                    and url not in self.all_urls:
                    # si oui, on ajoute dans l'output et on va chercher les liens
                    self.all_urls.append(url)
                    print('URL {} has been added to the index, now getting new URLs from this page'.format(url))
                    time.sleep(self.sleep_time) # mais on attend quelques secondes
                    current_page_content = self.get_page_content(url)
                    current_page_found_urls = self.find_urls(current_page_content)
                    print("{} URLs have been found on page {}".format(len(current_page_found_urls), url))
                    # si elle n'y est pas déjà, on rajoute chaque nouvelle url trouvée à next_urls
                    if len(current_page_found_urls) != 0: # si on a trouvé des urls
                        for new_url in current_page_found_urls:
                            if new_url not in self.next_urls:
                                self.next_urls.append(new_url)
                # on retire l'url qui vient d'être vérifiée (voire crawlée) de la liste à la fin
                self.next_urls.remove(url)
                print('Current number of URLs in the index: {}'.format(len(self.all_urls)))

        return self.all_urls


    def save_results(self):
        """ Save all URLs found by the crawler in a text file """
        with open('crawled_webpages.txt', 'w') as f:
            f.writelines([url + '\n' for url in self.all_urls])
